! #CV-761

! #CV-691
ebay.de#$#hide-if-matches-xpath './/a[@*[contains(.,"UnifiedRankingScenario")]]/div[@class]//img[@src]/ancestor-or-self::li[starts-with(@class,"s-item")]'

! Popup/Popunder/Clickunder
frauporno.com#$#abort-current-inline-script document.querySelectorAll popMagic
aniworld.to#$#abort-current-inline-script JSON.parse /atob|break/
yabeat.org#$#abort-on-property-read __meteor_runtime_config__
xnxx-sexfilme.com#$#abort-on-property-read open; abort-on-property-write SpacialUp;

! MISC
celleheute.de#$#hide-if-matches-xpath '//*[@data-image-info]/img[contains(@style,"440")]/ancestor::div[@id][@class][1]//parent::div[@data-hook]/parent::div[@id and @class]'; hide-if-matches-xpath './/a[@data-testid]/*[@data-image-info]/img[contains(@style,"300")]/ancestor::a[@target]/parent::div[@id][@class][1]'; hide-if-contains-visible-text /Anzeige/ 'p>span[class][style]';
freenet.de#$#hide-if-matches-xpath '//div[@class]/img[@referrerpolicy]/ancestor::div[@class][1]'
xnxx-sexfilme.com#$#hide-if-matches-xpath './/span[contains(text(),"Advertisement")]/parent::div[@class][1]'; hide-if-matches-xpath './/div[contains(@class,"logo")]/ancestor::div[@data-ga_action="join"]/ancestor::aside[@id][1]';
||xnxx-sexfilme.com/*==/^$domain=xnxx-sexfilme.com
||xnxx-sexfilme.com/*froload.js?v=2^$domain=xnxx-sexfilme.com
||pushpad.xyz^$third-party,domain=xnxx-sexfilme.com
